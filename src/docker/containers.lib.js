'use strict';
var DockerAPI = require("./docker.lib"),
    docker = new DockerAPI();

var DockerContainers = class DockerContainers{
  
  constructor(){
  }
  
  start(id){
  	return new Promise(function(resolve, reject){
  		let options = {
  			path: `/containers/${id}/start`,
  			method: "post"
  		};
  		docker.request( options, function(error){
  			console.dir(error);
  			reject(error);
  		}, function(response){
  			resolve(response);
  		});
  	});
  }
  
  logs(params){
  	return new Promise(function(resolve, reject){
  		let	options = {
  			path: `/containers/${params.id}/logs?stderr=1&stdout=1&timestamps=1&follow=1&tail=10&since=1428990821`,
  			json: false
  		};

  		docker.streamRequest( options, params, function(error){
  			console.error(error);
  		}, function(response){
  			resolve(response);
  		});
  	});
  }
  
  stats(id, event){
	let d = new DockerAPI();
  	 return new Promise(function(resolve, reject){
          let options = {
            path: `/containers/${id}/stats`
          },
          params = {id: id, streamer: event, eventName: "statsstream"};
          d.streamRequest( options, params, function(error){
                console.log(error);
                reject(error);
          }, function(response){
          			response.id = id;
                resolve( response );
          });
      });
  }
  
  findAll(){
      let _this = this;
      return new Promise(function(resolve, reject){
          let options = {
            path: '/containers/json?all=true'
          };
          docker.request( options, function(error){
                console.log(error);
                reject(error);
          }, function(response){
                resolve(response);
          });
      });
  }
      
};

module.exports = DockerContainers;