'use strict';

var http = require("http"),
    _ = require("underscore"),
  sockaddr = '/var/run/docker.sock';
  
 var DockerAPI = class DockerAPI{
     constructor(){
        this.defaultOptions = {
            socketPath: sockaddr,
            method: 'GET',
            headers: {
              'Accept': 'application/json'
            }
        };
     }
     
    streamRequest(options, params, err, cb){
    	let request,
            response = [],
            requestOptions = _.extend( this.defaultOptions, options );
        
        request = http.request(requestOptions, function(res){
            if (res.statusCode === 200) {
              res.setEncoding('utf8');
              res.on('data', function(chunk) {
            	let parse = (typeof requestOptions.json !== "undefined" && requestOptions.json == false) ? false : true,
            		stream;
				if(parse){
					try{
						stream = JSON.parse(chunk)
						stream.id = params.id;
					}
					catch(e){
						stream = {id: params.id, data: chunk }; // chunk + "<br/>";
					}
				} else{
					stream = {id: params.id, data: chunk }; // chunk + "<br/>";
				}
                params.streamer.sender.send(params.eventName, stream, chunk);
              });
            } else {
              console.error('ERROR: status:', res.statusCode);
              err(res.statusCode);
            }
            
            res.on('end', function(){
                var r = response.join();
                cb(JSON.parse(r));
            })
        });
        request.end();
    }
     
    request(options, err, cb){
        let request,
            response = [],
            requestOptions = _.extend( this.defaultOptions, options );
        
        request = http.request(requestOptions, function(res){
            if (res.statusCode === 200) {
              res.setEncoding('utf8');
              res.on('data', function(chunk) {
                response.push(chunk);
              });
            } else {
              console.error('ERROR: status:', res.statusCode);
              err(res.statusCode);
            }
            
            res.on('end', function(){
                var r = response.join();
                try{
                	cb(JSON.parse(r));
                }
                catch(e){
                	console.error("Error: dockerlib request before callback" + e.message);
                	console.log(JSON.stringify(requestOptions));
                	console.log(r);
                }
            })
        });
        request.end();
    }
 };
 
 module.exports = DockerAPI;