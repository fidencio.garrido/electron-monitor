/* global can, $, location, log */

window.ipcRenderer.on("containers", (event, containersList) => {
	containers.rendercontainers(containersList);
});

window.ipcRenderer.on("statsstream", (event, data) => {
	containers.renderstats(data);
});

window.ipcRenderer.on("logsstream", (event, data) => {
	log.append(data);
});

"use strict";
var refresher,
	openItems;


var Containers = can.Control.extend({
	statsSeries: {
		/**
		 * containerId: { mem: x, proc: x, tx: x, err: x, avgResTime }, alternative: {reqsPerSecond}
		 */ 
	},
	".cmd-section click": function(element, event){
		event.preventDefault();
		let section = $(element).data("target"),
			cid = $(element).data("id"),
			command = $(element).data("command"),
			cname = $(element).data("name");
		$(".dsection").hide();
		$(`#${section}`).show();
		this.command({msg: command, id: cid});
		this.handlers[command](cid, cname);
	},
	handlers: {
		logs(cid, cname){
			log.addLogTab(cid, cname);
		}	
	},
    ".cmd-navigate click": function(element, event){
        let sel = ".dsection",
            show = $(element).data("target"),
            active = "active";
        event.preventDefault();
        $(sel).hide();
        $(".cmd-navigate").removeClass( active );
        $(element).addClass( active );
        $(`.${show}`).show();
    },
    ".actions click": function(element, event){
        event.preventDefault();
    	let id = $(element).data("id");
        this.streamstats(id);
        this.flip(element, ".c-info", ".actions-panel");
    },
    ".c-action click": function(element, event){
    	event.preventDefault();
    	let _id = $(element).parent().data("id"),
    		command = $(element).data("command");
    	this.command( {msg: command, id: _id });
    },
    ".actions-close click": function(element, event){
        event.preventDefault();
        this.flip(element, ".actions-panel", ".c-info");
    },
    flip: function(element, hide, show){
        let id = $(element).data("id");
        this.findAllMatches(hide, id, (item)=>$(item).hide());
        this.findAllMatches(show, id, (item)=>$(item).show());
    },
    findAllMatches: function(sel, id, callback){
        $(sel).each(function(){
            if ($(this).data("id") === id)
                callback(this);
        });
    },
    rendercontainers: function(_containers){
    	$(".containers").html( can.view("resources/mustaches/containers", {containers: _containers}) );
    },
    renderstats: function(_stats){
    	// Move this
    	let cpuDelta = _stats.cpu_stats.cpu_usage.total_usage - _stats.precpu_stats.cpu_usage.total_usage;
        let systemDelta = _stats.cpu_stats.system_cpu_usage - _stats.precpu_stats.system_cpu_usage;
        let size = cpuDelta / systemDelta * 100;
        // pass this to stats?
        let formatted = size.toFixed(2);
        
    	let snapshot = {
    		mem: (_stats.memory_stats.usage / _stats.memory_stats.limit) *100,
    		txErr: _stats.networks.eth0.tx_errors,
    		txBytes: _stats.networks.eth0.tx_bytes,
    		cpu: formatted
    	};
    	if (!this.statsSeries[_stats.id]){
    		this.statsSeries[_stats.id] = [];
    	}
    	this.statsSeries[_stats.id].push(snapshot);
    	// End move
    	$(`#c-info-${_stats.id}`).html( can.view("resources/mustaches/stats", {stats: _stats}) );
    },
    command: function( msg ){
    	window.ipcRenderer.send("docker", msg);	
    },
    getcontainers: function(){
    	window.ipcRenderer.send("docker", "containers");
    },
    streamstats: function(_id){
    	window.ipcRenderer.send("docker", {msg: "stats", id: _id});
    },
    renderCharts(){
    	let keys = Object.keys(this.statsSeries);
    	for(let cont of keys){
    		console.log(cont,this.statsSeries[cont]);
    		let chart = d3.selectAll(`.c-smoke-${cont}>svg>circle`);
    		let net = d3.selectAll(`.c-smoke-${cont}>svg>rect.cpu`);
    		let mem = d3.selectAll(`.c-smoke-${cont}>svg>rect.mem`);
    		console.log(chart);
    		chart.style("fill", "steelblue");
    		let stats = this.statsSeries[cont].pop(),
    			c = stats.cpu,
    			m = stats.mem;
    		console.log("d:",c,m);
    		net.data([c]);
    		net.attr("width", function(c) { 
    			// 710
    			// 710-100
    			// x  - d
    			console.log("val",c);
    			return (c*710)/100; 
    			
    		});
    		console.log("here");
    		mem.data([m]);
    		mem.attr("width", function(m) {
    			console.log("mem",m);
    			return (m*710)/100; 
    		});
    		
    		//if !chart
    			// createChart
    		//addDataToChart or bind?
    	}
    },
    init: function(){
    	var _this = this;
    	this.getcontainers();
        refresher = setInterval(function(){
        	openItems = {};
        	$(".actions-panel:visible").each(function(){
        		let id = $(this).data("id");
        		openItems[id] = true;
        	});
        	let ids = Object.keys(openItems);
        	for (let id of ids){
        		_this.streamstats( id );
        	}
        }, 5000);
    }
});


var containers = new Containers(".docker-section", {});

let charter = setInterval(containers.renderCharts.bind(containers), 2000);