const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;
const Docker = require("./src/docker/containers.lib");

var mainWindow = null;

ipcMain.on("docker", (event, arg) => {
	if (arg === "containers"){
		var docker = new Docker();
		docker.findAll().then(function(c){
			event.sender.send("containers", c);
		});
	}
	if (typeof arg === "object"){
		var docker = new Docker();
		switch (arg.msg){
			case "stats":
				docker.stats(arg.id, event).then(function(c){
					//event.sender.send("statsstream", c);
				});
				break;
			case "logs":
				let params = {id: arg.id, streamer: event, eventName: "logsstream"};
				docker.logs(params).then(function(c){
					
				});
				break;
			case "start":
				docker.start(arg.id).then(function(){
					event.sender.send("refresh", arg.id);	
				});
				break;
		}
	}
});

app.on("ready", ()=> {
	mainWindow = new BrowserWindow({width: 1300, height: 600, webPreferences: {nodeIntegration: false, preload: __dirname + '/preload.js'} });
	mainWindow.loadURL("file://"+__dirname+"/index.html");
	
});