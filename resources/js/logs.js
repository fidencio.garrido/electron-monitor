/* global can, $ */
"use strict";

var Logs = can.Control.extend({
	/**
	 * classes
	 *
	 */
	c: {
		active: "active"	
	},
	/**
	 * selectors
	 *
	 */
	s: {
		lognav: ".log-navigate"	
	},
	tabs: {
		
	},
	".log-navigate click"(element, event){
		event.preventDefault();
		$(this.s.lognav).parent().removeClass( this.c.active );
		$(element).parent().addClass("active");
		let id = $(element).data("id");
		this.display(id);
	},
	append(data){
		let containerid = data.id,
			chunk = data.data;
		this.element.find(".logterminals").find(`.log-${containerid}`).append(chunk+"<br/>");
	},
	_renderNewTab(containerid, containername){
		$(this.s.lognav).parent().removeClass( this.c.active );
		this.element.find(".logtabs").append( can.view("resources/mustaches/logtab", {id: containerid, label: containername} ) );
		this.element.find(".logterminals").append( can.view("resources/mustaches/log", {id: containerid, label: "Log window"} ) );
		this.display(containerid);
	},
	display(containerid){
		let sel = ".log-terminal";
		$(sel).hide();
		$(`.log-${containerid}`).show();
	},
	addLogTab (containerid, containername){
		if (this.tabs[containerid]){
			this.display(containerid);
		} else{
			this.tabs[containerid] = true;
			this._renderNewTab(containerid, containername);
		}
	},
	init (el){
		this.element = el;
		console.log("Log manager created");
	}
});

var log = new Logs("#logs", {});