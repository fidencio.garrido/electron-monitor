/* global can $ */
"use strict";

function removeSlash(item){
	if (item.indexOf("/")===0)
            item = item.slice(1,item.length);
        return item;
}

can.mustache.registerHelper("pickOne",
	function(names){
		return removeSlash(names[0]);
	}
);

can.mustache.registerHelper("removeSlash",
    function(item){
        return removeSlash(item);
    }
);

can.mustache.registerHelper("shortId", 
    function(id){
        let shortId = id.split(":")[1].slice(0,12);
        return shortId;
    }
);

can.mustache.registerHelper("shortIfNeeded", 
    function(id){
        id = (typeof id!=="undefined") ? id : "";
        let shortId = id;
        if (id.indexOf("sha256") == 0)
            shortId = id.split(":")[1].slice(0,12);
        return shortId;
    }
);

can.mustache.registerHelper("reptag",
    function(fullName, partNdx){
        let part = fullName[0].split(":")[partNdx];
        return part;
    }
);

can.mustache.registerHelper("formatDate",
    function(date){
        let formatted = new Date(date * 1000);
        formatted = formatted.toLocaleDateString("en-US");
        return formatted;
    }
);

can.mustache.registerHelper("formatSize", 
    function(size){
        let formatted = ((size / 1024) / 1024).toFixed(2);
        return `${formatted} MB`;
    }
);

can.mustache.registerHelper("ratio", 
    function(used, max){
        let size = (used / max) * 100;
        let formatted = size.toFixed(2);
        return `${formatted}`;
    }
);

can.mustache.registerHelper("toMegabytes", 
    function(size){
        let formatted = (size / 1024).toFixed(2);
        return `${formatted} MB`;
    }
);

can.mustache.registerHelper("link",
    function(ports, networkSettings){
        let ip = networkSettings.Networks.bridge.IPAddress,
            localPort = "",
            remotePort = "",
            links = "";
        for(let ndx in ports){
            localPort = ports[ndx].PrivatePort;
            remotePort = ports[ndx].PublicPort;
        }
        let host = window.location.host.split(":")[0];
        links = (typeof localPort !== "string") ? can.mustache.safeString(`<br><a href="http://${ip}:${localPort}" target="blank">${localPort}</a> | <a href="http://${host}:${remotePort}" target="blank">${remotePort}</a>`) : "";
        return links;
    }
);

can.mustache.registerHelper("cpu", 
    function(total_cpu, total_usage, system_usage, system_pre_usage){
        let cpuDelta = total_cpu - total_usage;
        let systemDelta = system_usage - system_pre_usage;
        let size = cpuDelta / systemDelta * 100;
        let formatted = size.toFixed(2);
        return `${formatted} %`;
    }
);